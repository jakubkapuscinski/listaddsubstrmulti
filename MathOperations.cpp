#include "MathOperations.h"
List Sum(List a, List b)
{
	List result = List();
	List::Node* succA = a.tail;
	List::Node* succB = b.tail;
	int dozens = 0, sum = 0;

	if ((a.isNumberNegative && !b.isNumberNegative) || (!a.isNumberNegative && b.isNumberNegative))
	{
		if (a.isNumberNegative)
		{
			a.isNumberNegative = false;
			return Substr(b, a);
		}
		else
		{
			b.isNumberNegative = false;
			return Substr(a, b);
		}
	}
	while ((succA != NULL) || (succB != NULL))
	{
		sum = dozens + (succA ? succA->value : 0) +
			(succB ? succB->value : 0);

		dozens = (sum >= 10) ? 1 : 0;

		sum = sum % 10;

		result.Push(to_string(sum));

		if (succA) succA = succA->prev;
		if (succB) succB = succB->prev;

	}
	if (dozens > 0)
		result.Push(to_string(dozens));

	if (a.isNumberNegative && b.isNumberNegative)
	{
		List::Node* temp = result.head;
		result.Remove();
		result.Push("-" + to_string(temp->value));
	}
	return result;
}


List Substr(List a, List b)
{
	List result = List();
	int carry = 0, diff = 0;
	bool isNegative;

	if ((!a.isNumberNegative && b.isNumberNegative))
	{
		b.isNumberNegative = false;
		result = Sum(a, b);
	}
	else if (a.isNumberNegative && !b.isNumberNegative)
	{
		a.isNumberNegative = false;
		result = Sum(a, b);
		result.isNumberNegative = true;
	}
	else if (a.isNumberNegative && b.isNumberNegative)
	{
		b.isNumberNegative = false;
		result = Sum(a, b);
	}
	else //przypadek dla dodatnich
	{
		List::Node* succA;
		List::Node* succB;
		
		//sprawdzenie ktora z liczb jest mniejsza i ustawienie 
		if (a.counter < b.counter)
		{
			succA = b.tail;
			succB = a.tail;
			isNegative = true;
		}
		else if (a.counter > b.counter)
		{
			succA = a.tail;
			succB = b.tail;
			isNegative = false;
		}
		else
		{
			List::Node* tmpA = a.head;
			List::Node* tmpB = b.head;

			while (tmpA != NULL && tmpB != NULL && tmpA->value == tmpB->value)
			{
				tmpA = tmpA->next;
				tmpB = tmpB->next;

			}
			if (tmpA != NULL && tmpB != NULL && tmpA->value < tmpB->value)
			{
				succA = b.tail;
				succB = a.tail;
				isNegative = true;
			}
			else
			{
				succA = a.tail;
				succB = b.tail;
				isNegative = false;
			}
		}

		while (succB != NULL)
		{
			if (succA->value < succB->value)
			{
				List::Node* temp = succA->prev;
				while (temp->value == 0)
				{
					temp = temp->prev;
				}
				while (temp != succA)
				{
					temp->value--;
					temp = temp->next;
					temp->value += 10;
				}
			}

			diff = succA->value - succB->value;

			result.Push(to_string(diff));

			if (succA) succA = succA->prev;
			if (succB) succB = succB->prev;
		}

		while (succA != NULL)
		{
			result.Push(to_string(succA->value));
			succA = succA->prev;
		}

		while (result.head->value == 0)
		{
			result.Remove();
			if (result.head->next == NULL) break;
		}

		int sum = 0;
		List::Node* walker = result.head;
		while (walker != NULL)
		{
			sum += walker->value;
			walker = walker->next;
		}

		if (sum == 0)
		{
			while (result.head != result.tail)
			{
				result.Remove();
			}
		}

		if (isNegative)
		{
			List::Node* temp = result.head;
			result.Remove();
			result.Push("-" + to_string(temp->value));
		}
		return result;
	}

	if (result.isNumberNegative)
	{
		List::Node* temp = result.head;
		result.Remove();
		result.Push("-" + to_string(temp->value));
	}

	return result;
}

List Multiply(List a, List b)
{
	List::Node* succA;
	List::Node* succB;
	//wybieram ktora z liczb "bedzie wyzej" 
	if (a.counter < b.counter)
	{
		succA = b.tail;
		succB = a.tail;
	}
	else
	{
		succA = a.tail;
		succB = b.tail;
	}

	const int cnt = b.counter;
	List* lists = new List[cnt];

	int counter = 0;
	while (succB != NULL) // dop�ki mniejsza liczba sie nie skonczy
	{
		int dozens = 0, sum = 0;
		List::Node* tmp = succA;

		for (int i = 0; i < counter; i++)
		{
			lists[counter].Push("0");
		}

		while (tmp != NULL) // dopoki wieksza liczba sie nie skonczy
		{
			sum = (tmp ? tmp->value : 0) *
				(succB ? succB->value : 0) + dozens;

			dozens = sum / 10;

			sum = sum % 10;
			lists[counter].Push(to_string(sum));

			if (tmp) tmp = tmp->prev;

		}
		if (dozens != 0) lists[counter].Push(to_string(dozens));
		counter++;

		if (succB) succB = succB->prev;
	}

	List tmp = lists[0];
	for (int i = 1; i < cnt; i++)
	{
		List tmp2 = Sum(tmp, lists[i]);
		tmp = tmp2;
	}

	if (((a.isNumberNegative && tmp.head->value != 0) && (!b.isNumberNegative && tmp.head->value != 0))
		|| (!a.isNumberNegative && tmp.head->value != 0) && (b.isNumberNegative && tmp.head->value != 0))
	{
		List::Node* temp = tmp.head;
		tmp.Remove();
		tmp.Push("-" + to_string(temp->value));
	}

	if (tmp.head->value == 0)
	{
		int sum = 0;
		List::Node* walker = tmp.head;
		while (walker != NULL)
		{
			sum += walker->value;
			walker = walker->next;
		}

		if (sum == 0)
		{
			while (tmp.head != tmp.tail)
			{
				tmp.Remove();
			}
		}
	}

	return tmp;
}
