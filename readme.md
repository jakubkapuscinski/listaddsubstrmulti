
## Implementacja Matematyki Wielkich Liczb w języku C++

- Program napisany w języku C++, który przeprowadza działania dodawania, odejmowania i mnożenia podanych dwóch liczb całkowitych wzorując się na metodzie pisemnej. 
- W tym celu stworzona została autorska struktura listy , w której przechowywane są kolejno cyfry każdej z liczb (czynników działania). 
- W zależności od wybranego przez użytkownika aplikacji działania, wykonane zostaje dodawanie/odejmowanie/mnożenie dwóch podanych przez niego liczb, zamienionych na dwie listy. 
- Taki sposób przechowywania danych umożliwia operacje na ogromnych liczbach bez konieczności kontrolowania ich typów (int, long itd.), gdyż oba czynniki, jak i wynik działania są listami. Jest to tzw. Matematyka Wielkich Liczb.