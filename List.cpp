#include "List.h"
#include <iostream> 

List::List()
{
	head = tail = nullptr;
	isNumberNegative = false;
	counter = 0;
}

List::List(string l)
{
	head = tail = nullptr;
	counter = 0;
	isNumberNegative = false;
	unsigned int i = 0;

	if (l[0] == '-')
	{
		isNumberNegative = true;
		i++;
	}
	while (i < l.size())
	{
		Insert(string(1, l[i]));
		i++;
	}
}

void List::Insert(string v)
{
	Node* pred = nullptr;
	Node* succ = head;

	while (succ != nullptr)
	{
		pred = succ;
		succ = succ->next;
	}

	Node* creator = new Node(v, succ, pred);

	if (pred == nullptr)
	{
		head = creator;
		tail = creator;
	}
	else
		pred->next = creator;
	tail = creator;

	counter++;
}

void List::Push(string v)
{
	Node* pred = nullptr;
	Node* succ = tail;

	while (succ != nullptr)
	{
		pred = succ;
		succ = succ->prev;
	}

	Node* creator = new Node(v, pred, succ);

	if (pred == nullptr)
	{
		head = creator;
		tail = creator;
	}
	else
		pred->prev = creator;
	head = creator;

	counter++;
}

void List::Remove()
{
	head = head->next;

	if (head)
	{
		head->prev = nullptr;
	}

	counter--;
}

bool List::isEmpty()
{
	if (head == nullptr)
	{
		return true;
	}
	return false;
}

int List::GetLength()
{
	return counter;
}

string List::ToString()
{
	string result = "";
	Node* n = head;

	for (int i = counter; i > 0; i--)
	{
		result += to_string(n->value);
		n = n->next;
	}

	return result;
}
