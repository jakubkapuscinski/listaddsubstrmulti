
#include <string>
using namespace std;

struct List
{
public:
	struct Node
	{
		int value;
		Node* next;
		Node* prev;

		Node(string v, Node* n, Node* p)
		{
			value = stoi(v);
			next = n;
			prev = p;
		}
	};

	Node* head;
	Node* tail;
	int counter;
	bool isNumberNegative;

public:
	List();
	List(string);
	void Insert(string v);
	void Push(string v);
	void Remove();
	bool isEmpty();
	int GetLength();
	string ToString();
};